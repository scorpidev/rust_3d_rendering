use sdl2::{rect::Point, render::{Canvas, Texture}};
use sdl2::video::Window;
use sdl2::pixels::Color;
use std::mem;

use crate::{WINDOW_HEIGHT, WINDOW_WIDTH, geometry::Vector3D};

pub fn triangle(mut v0: Vector3D<i32>, mut v1: Vector3D<i32>, mut v2: Vector3D<i32>, canvas: &mut Canvas<Window>, z_buffer: &mut Vec<Vec<i32>>, color: u8) {
    
    canvas.set_draw_color(Color::RGB(color, color, color));
    // canvas.draw_line(Point::new(v0.x, WINDOW_HEIGHT as i32 - v0.y + 1), Point::new(v1.x, WINDOW_HEIGHT as i32 - v1.y + 1)).unwrap();
    // canvas.draw_line(Point::new(v1.x, WINDOW_HEIGHT as i32 - v1.y + 1), Point::new(v2.x, WINDOW_HEIGHT as i32 - v2.y + 1)).unwrap();
    // canvas.draw_line(Point::new(v2.x, WINDOW_HEIGHT as i32 - v2.y + 1), Point::new(v0.x, WINDOW_HEIGHT as i32 - v0.y + 1)).unwrap();
    
    if v0.y==v1.y && v0.y==v2.y {
        return; // i dont care about degenerate triangles
    }
    
    if v0.y > v1.y {
        mem::swap(&mut v0, &mut v1);
    }
    if v0.y > v2.y {
        mem::swap(&mut v0, &mut v2);
    }
    if v1.y > v2.y {
        mem::swap(&mut v1, &mut v2);
    }
    let total_height = v2.y - v0.y;
    for i in 0..total_height {
        let second_half = i > v1.y - v0.y || v1.y == v0.y;
        let segment_height = if second_half { v2.y - v1.y } else { v1.y - v0.y };
        let alpha = i as f32/total_height as f32;
        let beta  = (i - if second_half { v1.y - v0.y } else { 0 }) as f32/segment_height as f32; // be careful: with above conditions no division by zero here
        let mut a = v0.to::<f32>() + (v2-v0).to::<f32>()*alpha;
        let mut b = if second_half { v1.to::<f32>() + (v2-v1).to::<f32>()*beta } else { v0.to::<f32>() + (v1-v0).to::<f32>()*beta };
        if a.x>b.x{
            mem::swap(&mut a, &mut b);
        }
        for j in a.x as i32..b.x as i32+1 {
            let phi = if b.x == a.x { 1. } else { (j as f32 - a.x)/(b.x - a.x) };
            let p = (a + (b-a)*phi).to::<i32>();
            if z_buffer[p.x as usize][p.y as usize] < p.z {
                z_buffer[p.x as usize][p.y as usize] = p.z;
                canvas.draw_point(Point::new(p.x, WINDOW_HEIGHT as i32 - (p.y+1))).unwrap()
            }
            

            // self.set(j, y0+i, color); // attention, due to int casts t0.y+i != A.y
        }
    }
}