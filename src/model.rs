use super::geometry::Vector3D;
use std::fs::File;
use std::io::{BufRead, BufReader};
use std::path::Path;

pub struct Model {
    pub vertices: Vec<Vector3D<f32>>,
    pub faces: Vec<[i32; 3]>,
}

impl Model {
    pub fn new(file_path: &str) -> Model {
        let path = Path::new(file_path);
        let file = BufReader::new(File::open(&path).unwrap());
        let mut vertices = Vec::new();
        let mut faces = Vec::new();
        for line in file.lines() {
            let line = line.unwrap();
            let words: Vec<&str> = line.split_whitespace().collect();
            if line.starts_with("v ") {
                vertices.push(Vector3D::new(words[1].parse().unwrap(), 
                                            words[2].parse().unwrap(),
                                            words[3].parse().unwrap()));
            } else if line.starts_with("f ") {
                let mut face: [i32; 3] = [-1, -1, -1];
                for i in 0..3 {
                    face[i] = words[i+1].split("/").next().unwrap().parse().unwrap();
                    face[i] -= 1;
                }
                faces.push(face);
            }
        }
        Model {
            vertices,
            faces,
        }
    }
}