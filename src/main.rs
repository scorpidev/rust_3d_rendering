extern crate sdl2;

use geometry::Vector3D;
use sdl2::{event::Event, pixels::{Color, PixelFormatEnum}};
use sdl2::rect::Point;
use rand::Rng;
use sdl2::keyboard::Keycode;

mod model;
mod geometry;
mod drawing;
mod tga_canvas;

use model::Model;
use drawing::triangle;

const WINDOW_WIDTH: u32 = 800;
const WINDOW_HEIGHT: u32 = 800;


fn get_gray(intensity: f32) -> u32 {
    let mut result = (255.0*intensity) as u32;
    result += (255.0*intensity) as u32*256;
    result += (255.0*intensity) as u32*256*256;
    return result;
}


fn main() {
    let sdl_context = sdl2::init().unwrap();

    let video_subsystem = sdl_context.video().unwrap();
    let mut sdl_event_context = sdl_context.event_pump().unwrap();

    let window = video_subsystem.window("3D Rendering", WINDOW_WIDTH, WINDOW_HEIGHT)
        .position_centered()
        .vulkan()
        .build()
        .unwrap();
    
    let mut canvas = window.into_canvas().present_vsync().build().unwrap();

    let light_direction = Vector3D::new(0.0, 0.0, -1.0);

    let model = Model::new("african_head.obj");

    let mut running = true;

    while running {
        canvas.set_draw_color(Color::RGB(255, 255, 255));
        canvas.clear();
        canvas.set_draw_color(Color::RGB(0, 0, 0));
        let mut z_buffer = vec![vec![std::i32::MIN; WINDOW_HEIGHT as usize]; WINDOW_WIDTH as usize];
        for face in &model.faces {
            let mut p: [Vector3D<i32>; 3] = [Vector3D::new(0, 0, 0); 3];
            let mut world_p: [Vector3D<f32>; 3] = [Vector3D::new(0.0, 0.0, 0.0); 3];
            for j in 0..3 {
                world_p[j] = model.vertices[face[j] as usize];

                p[j].x = ((world_p[j].x+1.)*WINDOW_WIDTH as f32/2.) as i32;
                p[j].y = ((world_p[j].y+1.)*WINDOW_HEIGHT as f32/2.) as i32;
                p[j].z = ((world_p[j].z+1.)*WINDOW_HEIGHT as f32/2.) as i32;
            }
            let n = (world_p[2] - world_p[0])^(world_p[1]-world_p[0]);
            let n = n.normalized(1.0);
            let intensity = n*light_direction;
            if intensity > 0.0 {
                triangle(p[0], p[1], p[2], &mut canvas, &mut z_buffer, get_gray(intensity) as u8)
            }
        }

        for event in sdl_event_context.poll_iter() {
            match event {
                Event::Quit {..} | Event::KeyDown {keycode: Some(Keycode::Escape), ..} => {
                    running = false;
                },
                _ => {}
            }
        }
        canvas.present();
    }

}
